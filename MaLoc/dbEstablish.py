#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @File  : dbEstablish.py
# @Author: HT
# @Date  : 2018/6/3
# @Desc  :

from util.readExcel import get_data_by_sheet
from util.meanFilter import mean_filter
from MapsFeatures import *
from util.readPKL import get_pkl_data

dist = 50


def generate_db_data(file, all_pos):
    orignal_data = get_data_by_sheet(file, sheet_index=0)
    mag_x = mean_filter(orignal_data[1][1:], filter_size=10)
    mag_y = mean_filter(orignal_data[2][1:], filter_size=10)
    mag_z = mean_filter(orignal_data[3][1:], filter_size=10)

    mag_x = mag_x - np.average(mag_x)
    mag_y = mag_y - np.average(mag_y)
    mag_z = mag_z - np.average(mag_z)

    db_data = []

    for i in np.arange(0, len(mag_x), dist):
        db_data.append([float(mag_x[i]), float(mag_y[i]), float(mag_z[i]),
                        all_pos[min(i, len(all_pos)-1)]])

    # f = open("db_data.txt", 'w')
    # for index in range(len(map_path)):
    #     path_start = map_path[index][0]
    #     path_end = map_path[index][1]
    #     pos_start = map_pos[index]
    #     pos_end = map_pos[index+1]
    #     x = mag_x[path_start:path_end:dist]
    #     y = mag_y[path_start:path_end:dist]
    #     z = mag_z[path_start:path_end:dist]
    #     for i in range(len(x)):
    #         pos_x = pos_start[0] + (pos_end[0] - pos_start[0]) * (i / len(x))
    #         pos_y = pos_start[1] + (pos_end[1] - pos_start[1]) * (i / len(x))
    #         # f.write("%.2f %.2f %.2f %.2f %.2f\n" % (float(x[i]), float(y[i]), float(z[i]), pos_x, pos_y))
    #         db_data.append([float(x[i]), float(y[i]), float(z[i]), pos_x, pos_y])

    return db_data


def generate_db_data_2(sensor_data, loc_start_data, loc_end_data):
    db_data = []

    for i in range(len(sensor_data)):
        loc_start = np.array(loc_start_data[i])
        loc_end = np.array(loc_end_data[i])
        mags = sensor_data[i, :, 0:3]
        for j in np.arange(0, len(mags), 10):
            mag = mags[j]
            loc = loc_start + (loc_end - loc_start) * (j / len(mags))
            db_data.append([mag, loc])
    return db_data


# generate_db_data("C:/Users/justk/Desktop/magData/lab/train.xlsx")
