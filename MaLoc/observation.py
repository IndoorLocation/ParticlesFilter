#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @File  : observation.py
# @Author: HT
# @Date  : 2018/6/3
# @Desc  :


def get_obv_pos(db_data, mag=[]):
    idx = 0
    dist = 10000
    for i in range(len(db_data)):
        item = db_data[i][0]
        tmp_dist = (float(item[0]) - float(mag[0]))**2 + (float(item[1]) - float(mag[1]))**2 + (float(item[2]) - float(mag[2]))**2
        if tmp_dist < dist:
            dist = tmp_dist
            idx = i
        else:
            continue

    return db_data[idx][1]


def get_obv_mag(db_data, pos=[]):
    idx = 0
    dist = 10000
    for i in range(len(db_data)):
        item = db_data[i]
        tmp_dist = (float(pos[0]) - float(item[3]))**2 + (float(pos[1]) - float(item[4]))**2
        if tmp_dist < dist:
            dist = tmp_dist
            idx = i
        else:
            continue

    return db_data[idx][0:3]


def get_obv_mags(db_data, particles_pos):
    mags = []
    for j in range(len(particles_pos)):
        pos = particles_pos[j]
        idx = 0
        dist = 10000
        for i in range(len(db_data)):
            item = db_data[i]
            tmp_dist = (float(pos[0]) - float(item[1][0])) ** 2 + (float(pos[1]) - float(item[1][1])) ** 2
            if tmp_dist < dist:
                dist = tmp_dist
                idx = i
            else:
                continue

        mags.append(db_data[idx][0])

    return mags
