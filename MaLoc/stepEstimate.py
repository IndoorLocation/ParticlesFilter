#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @File  : stepEstimate.py
# @Author: HT
# @Date  : 2018/5/26
# @Desc  : dynamic step length estimation


import numpy as np
import math
from util.meanFilter import mean_filter
from util.peakdetect import peakdetect


def step_lens_norm(loc, scale=1.0, size=None):
    return np.random.normal(loc, scale, size=size)


def step_detect(acc):
    acc = np.array(mean_filter(acc, filter_size=30) - np.average(acc, axis=0))
    res = peakdetect(acc, lookahead=25)
    peaks = np.array(res[0])
    # vallys = np.array(res[1])

    peaks_average = np.average(peaks[:, 1], axis=0)
    # vallys_average = np.average(vallys[:, 1], axis=0)

    peaks_filter = []
    vallys_filter = []

    for i in range(len(peaks)):
        if peaks[i][1] > peaks_average:
            peaks_filter.append(peaks[i])
    # for j in range(len(vallys)):
    #     if vallys_average - vallys[j][1] > -0.5 * math.fabs(vallys_average):
    #         vallys_filter.append(vallys[j])

    peaks_filter = np.array(peaks_filter)
    # vallys_filter = np.array(vallys_filter)

    return peaks_filter[:, 0]
