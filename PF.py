#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @File  : PF.py
# @Author: HT
# @Date  : 2018/5/25
# @Desc  : particle filter design for MaLoc

import random
import numpy as np
import math
import scipy
import heapq
from numpy.random import uniform


TURN_NOISE = 0.01
FORWARD_NOISE = 1
WEIGHT_REDUCE = 0.01
WEIGHT_INTENSITY = 1
RESAMPLE_PARA = 0.65
RESAMPLE_RADIUS = 60
STEP_LEN = 12


def create_particles(num, x_range, y_range, flag):
    """set coordinate and heading angle information"""
    particles = np.empty((num, 3))
    particles[:, 0] = uniform(x_range[0], x_range[1], size=num)
    particles[:, 1] = uniform(y_range[0], y_range[1], size=num)

    if flag[0] == 0:
        particles[:, 2] = np.ones(num) * (np.pi / 2)
    else:
        particles[:, 2] = np.zeros(num)
    # particles[:, 2] = uniform(0, 2*np.pi, size=num)

    if flag[1] == 0:
        particles[:, 2] += np.pi

    return particles


def pos_invalid_detect(particles, x_range, y_range, invalid_area=[]):
    idx = ((particles[:, 0] < x_range[0])
           | (particles[:, 0] > x_range[1])
           | (particles[:, 1] < y_range[0])
           | (particles[:, 1] > y_range[1]))

    for area in invalid_area:
        idx = idx | ((particles[:, 0] > area[0][0])
                     & (particles[:, 0] < area[0][1])
                     & (particles[:, 1] > area[1][0])
                     & (particles[:, 1] < area[1][1]))

    return idx


def pos_judge(area, pos_x, pos_y):
    idx = (((((pos_x * area[0][0] + pos_y * area[0][1] + area[0][2]) < 0) & (area[0][0] < 0))
           | (((pos_x * area[0][0] + pos_y * area[0][1] + area[0][2]) > 0) & (area[0][0] >= 0)))
        & ((((pos_x * area[1][0] + pos_y * area[1][1] + area[1][2]) > 0) & (area[1][0] < 0))
          | (((pos_x * area[1][0] + pos_y * area[1][1] + area[1][2]) < 0) & (area[1][0] >= 0)))
        & ((pos_x * area[2][0] + pos_y * area[2][1] + area[2][2]) > 0)
        & ((pos_x * area[3][0] + pos_y * area[3][1] + area[3][2]) < 0))
    return idx


def pos_invalid_detect_1(particles, x_range, y_range, invalid_area):
    idx = ((particles[:, 0] < x_range[0])
           | (particles[:, 0] > x_range[1])
           | (particles[:, 1] < y_range[0])
           | (particles[:, 1] > y_range[1]))

    for area in invalid_area:
        idx = idx | pos_judge(area, particles[:, 0], particles[:, 1])

    return idx


def move_particles(particles, weights,  heading_angle, x_range, y_range, invalid_area=[], heading_len=[]):
    idx = np.array([True] * len(particles))
    particles_last = np.copy(particles)
    for i in range(100):
        if i == 0:
            particles[idx, 2] = (particles_last[idx, 2] + (float(heading_angle) + random.gauss(0.0, TURN_NOISE)))
            # if angle < 0:
            #     particles[:, 2] = angle % (-2.0 * np.pi)
            # else:
            #     particles[:, 2] = angle % (2.0 * np.pi)
        else:
            particles[idx, 2] = uniform(0, 2*np.pi, size=np.sum(idx))

        dist = np.array(heading_len)  # + random.gauss(0.0, FORWARD_NOISE)
        particles[idx, 0] = particles_last[idx, 0] + np.cos(particles[idx, 2]) * dist[idx]
        particles[idx, 1] = particles_last[idx, 1] + np.sin(particles[idx, 2]) * dist[idx]

        idx = pos_invalid_detect(particles, x_range, y_range, invalid_area)
        if np.sum(idx) == 0:
            break
        else:
            weights[idx] *= WEIGHT_REDUCE


def update_particles_1(particles, weights, obv_pos):
    dist = np.linalg.norm(np.array(particles[:, 0:2]) - np.array(obv_pos), axis=1)
    weights *= scipy.stats.norm(0, 5).pdf(dist)
    weights += 1.e-300
    weights /= sum(weights)


def update_particles_2(particles, weights, obv):
    dist = np.linalg.norm(np.array(particles[:, 0:2]) - np.array(obv), axis=1)
    for idx in range(len(weights)):
        weights[idx] *= np.e**(-(dist[idx]**2)/(2*WEIGHT_INTENSITY**2))
    weights += 1.e-300
    weights /= sum(weights)


def update_particles_3(particles, weights, obv_mags, mag, obv_pos):
    dist_1 = np.linalg.norm(np.array(obv_mags) - np.array(mag), axis=1)
    # dist_2 = np.linalg.norm(np.array(particles[:, 0:2]) - np.array(obv_pos), axis=1)
    weights *= scipy.stats.norm(0, 5).pdf(dist_1 * 1 + 0 * 0.5)
    weights += 1.e-300
    weights /= sum(weights)


def update_particles_4(particles, weights, dtw_dists, obv_pos):
    dist_1 = np.array(dtw_dists)
    dist_2 = np.linalg.norm(np.array(particles[:, 0:2]) - np.array(obv_pos), axis=1)
    weights *= scipy.stats.norm(0, 5).pdf(dist_1 * 0.5 + dist_2 * 0.5)
    weights += 1.e-300
    weights /= sum(weights)


def update_particles_5(particles, weights, obv_pos):
    dist = np.linalg.norm(np.array(particles[:, 0:2]) - np.array(obv_pos), axis=1)
    weights *= np.exp(-1.0 * dist / 0.88)
    weights += 1.e-300
    weights /= sum(weights)


def resample_particles(particles, weights, num, heading_angle, previous_res):
    re_particles = []
    re_weight = []
    new_num = int(num * (RESAMPLE_PARA * np.fabs(float(heading_angle) + random.gauss(0, 1)) / math.pi))
    new_num = new_num % num
    top_weights = heapq.nlargest(int(num) - int(new_num), weights)
    weights_list = weights.tolist()
    for weight in top_weights:
        idx = weights_list.index(weight)
        re_particles.append([float(particles[idx, 0]), float(particles[idx, 1]), float(particles[idx, 2])])
        re_weight.append(weights[idx])
        weights_list[idx] = -1

    new_particles = np.empty((new_num, 3))
    new_weights = np.ones(new_num)/num
    new_particles[:, 0] = uniform(previous_res[0] - RESAMPLE_RADIUS, previous_res[0] + RESAMPLE_RADIUS, size=new_num)
    new_particles[:, 1] = uniform(previous_res[1] - RESAMPLE_RADIUS, previous_res[1] + RESAMPLE_RADIUS, size=new_num)
    new_particles[:, 2] = uniform(min(previous_res[2], previous_res[2] + 2*heading_angle),
                                  max(previous_res[2], previous_res[2] + 2*heading_angle), size=new_num)

    return np.reshape(np.append(np.array(re_particles, dtype='float'), new_particles), (num, 3)), \
           np.append(np.array(re_weight, dtype='float'), new_weights, axis=0)


def estimate_max(particles, weights):
    return particles[np.where(weights==max(weights))][0]


def estimate_average(particles, weights):
    return np.average(particles, weights=weights, axis=0)

