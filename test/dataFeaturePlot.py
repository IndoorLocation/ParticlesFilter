#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @File  : dataFeaturePlot.py
# @Author: HT
# @Date  : 2018/6/9
# @Desc  :

from matplotlib import pyplot as plt
import numpy as np
import scipy.stats
from util.meanFilter import mean_filter
from util.peakdetect import peakdetect
from util.readExcel import get_data_by_sheet
import math
from MapsFeatures import *
from util.plot import plot_cdf, get_cdf_plt
from util.readPKL import get_pkl_data

file_path = lab_train_file


def mag_data_plot(file, scale=None, offset=None):
    if scale is None:
        scale = 1
    data = get_data_by_sheet(file)

    mag_x = data[1][1:]
    mag_y = data[2][1:]
    mag_z = data[3][1:]
    mag = np.sqrt(np.array(mag_x, dtype='float') ** 2
                  + np.array(mag_y, dtype='float') ** 2
                  + np.array(mag_z, dtype='float') ** 2)

    mag_filter = mean_filter(mag[0:len(mag):scale], filter_size=30)
    # plt.plot(np.array(mag_filter) - np.average(mag_filter))
    # plt.plot(mag)
    plt.plot(mag_filter)
    plt.show()


# mag_data_plot("C:/Users/justk/Desktop/magData/gogo/s8.xlsx")
# mag_data_plot("C:/Users/justk/Desktop/magData/path 0629-mi6.xlsx")


def acc_data_plot(file):
    data = get_data_by_sheet(file)
    acc_x = data[4][1:]
    acc_y = data[5][1:]
    acc_z = data[6][1:]
    acc = []
    for i in range(1, min(acc_x.__len__(), acc_y.__len__(), acc_z.__len__()), 1):
        acc.append(math.sqrt(acc_x[i]**2 + acc_y[i]**2 + acc_z[i]**2))
    acc_filter = np.array(mean_filter(acc, filter_size=30) - np.average(acc))
    acc_res = peakdetect(acc_filter, lookahead=40)
    acc_peaks = np.array(acc_res[0])
    acc_valleys = np.array(acc_res[1])

    peak_dist = []
    for i in range(len(acc_peaks)-1):
        peak_dist.append(acc_peaks[i+1][0] - acc_peaks[i][0])

    peaks_average = np.average(acc_peaks[:, 1], axis=0)
    vallys_average = np.average(acc_valleys[:, 1], axis=0)

    peaks_filter = []
    vallys_filter = []

    for i in range(len(acc_peaks)):
        if acc_peaks[i][1] - peaks_average > - 0.5 * peaks_average:
            peaks_filter.append(acc_peaks[i])
    for j in range(len(acc_valleys)):
        if vallys_average - acc_valleys[j][1] > - 0.5 * math.fabs(vallys_average):
            vallys_filter.append(acc_valleys[j])

    peaks_filter = np.array(peaks_filter, dtype='float')
    vallys_filter = np.array(vallys_filter, dtype='float')
    print(np.average(peak_dist))
    print(acc_peaks)
    plt.plot(acc_filter)
    plt.scatter(peaks_filter[:, 0], peaks_filter[:, 1], edgecolors='red')
    plt.scatter(vallys_filter[:, 0], vallys_filter[:, 1], edgecolors='red')
    # plt.plot(peak_dist)
    plt.show()


# acc_data_plot(humanity_train_file)
# acc_data_plot(humanity_test_file)
acc_data_plot("C:/Users/justk/Desktop/magData/gogo/gogomag/mate.xlsx")


def gyro_data_plot(file):
    data = get_data_by_sheet(file)
    gyro_x = data[7][1:]
    gyro_y = data[8][1:]
    gyro_z = data[9][1:]

    gyro = np.sqrt(np.array(gyro_x, dtype='float') ** 2 + np.array(gyro_y, dtype='float') ** 2 + np.array(gyro_z, dtype='float') ** 2)
    orien = data[4][1:]
    gyro_filter = np.array(mean_filter(gyro, filter_size=30))
    gyro_grad = []
    for i in range(len(gyro_filter) - 1):
        gyro_grad.append(gyro_filter[i+1] - gyro_filter[i])

    gyro_cumulative = []
    sum = 0
    for idx in range(len(gyro_z)):
        sum += gyro_z[idx]
        gyro_cumulative.append(sum)

    plt.plot(gyro_cumulative[0:1023])
    # plt.plot(gyro_grad)
    plt.show()


# gyro_data_plot(math_train_file)
# gyro_data_plot(math_test_file)
# gyro_data_plot("C:/Users/justk/Desktop/magData/gogo/c5.xlsx")


def location_error_plot(files):
    plt_data = []
    for i in range(len(files)):
        file = files[i]
        data = get_data_by_sheet(file)
        location_errors = []
        for i in range(len(data)):
            if str(data[i][0]) == "location_error":
                location_errors = data[i][1:]
                break
        location_errors = np.array(location_errors, dtype='float')
        plt_data.append(get_cdf_plt(location_errors, x_range=15, x_num=100))

    file_name = ["SeLiNa", "MaLoc", "Magicol", "TraviNavi"]
    plt.legend(handles=plt_data, labels=file_name, loc='lower right')
    plt.show()


def gogo_info_plot():
    file = "C:/Users/justk/Desktop/magData/gogo/chaosuan.pkl"
    sensor_data, loc_start_data, loc_end_data = get_pkl_data(file)
    # loc_data = loc_data[0:2525]
    plt.figure(1)
    plt.subplot(211)
    # plt.xlim(-60, 5)
    # plt.ylim(-20, 20)
    # plt.scatter(loc_end_data[0:2531, 0], loc_end_data[0:2531, 1], color='blue')
    plt.scatter(loc_start_data[50:1400, 0], loc_start_data[50:1400, 1], color='blue')
    plt.subplot(212)
    # plt.xlim(-60, 5)
    # plt.ylim(-20, 20)
    # plt.scatter(loc_end_data[2795:5328, 0], loc_end_data[2795:5328, 1], color='red')
    plt.scatter(loc_start_data[1450:, 0], loc_start_data[1450:, 1], color='red')
    plt.show()


# gogo_info_plot()



# location_error_plot(["C:\\Users\\\justk\\\Desktop\\\location error\\location_error_epoch_378_(20.09).xlsx",
#                     "C:\\Users\\\justk\\\Desktop\\\location error\\Maloc_math.xlsx",
#                     "C:\\Users\\\justk\\\Desktop\\\location error\\Magicol_math.xlsx",
#                     "C:\\Users\\\justk\\\Desktop\\\location error\\TraviNavi_math.xlsx"])
