#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @File  : MagicolTest.py
# @Author: HT
# @Date  : 2018/6/5
# @Desc  :

import PF
import time
import numpy as np
import random
import os
from Magicol import stepEstimate
from util.readExcel import get_data_by_sheet
from util.meanFilter import mean_filter
from Magicol.dbEstablish import generate_db_data_4
from Magicol.observation import get_obv_pos, get_obv_dists, get_obv_pos_dist
from matplotlib import pyplot as plt
from MapsFeatures import *
from DataProcess.generateTestData import generate_test_data
import statsmodels.api as sm
from util.readPKL import get_pkl_data

STEP_LENGTH = 15 / 20.0
STEP_ESTIMATE_NOISE = 0
STEP_PARA = 0.5
STEP_MAG_LEN = 30

map_invalid_area = supermarket_invalid_area
map_range = supermarket_range
map_name = "supermarket"

seg_len = 500
seg_interval = 50

step_num = 10

PLACE = "supermarket"
PHONE = "Mi_6"

# original data file path
TRAIN_DATA_PATH = "/home/hetao/Data/" + PLACE + "/" + PHONE + "/train/"
TEST_DATA_PATH = "/home/hetao/Data/" + PLACE + "/" + PHONE + "/test/"
RES_PATH = "/home/hetao/Data/" + PLACE + "/" + PHONE + "/res/PF/"

if __name__ == "__main__":
    # sensor_data, loc_start_data, loc_end_data = get_pkl_data(pkl_file_mate)
    # db_data = generate_db_data_3(sensor_data[0:2531:60], loc_start_data[0:2531:60], loc_end_data[0:2531:60], step_mag_len=STEP_MAG_LEN)
    # test_data = generate_test_data_gogo(sensor_data[2795:5328], loc_start_data[2795:5328], loc_end_data[2795:5328])

    db_data = generate_db_data_4(TRAIN_DATA_PATH)
    test_data = generate_test_data(TEST_DATA_PATH)
    # print(db_data)
    # print(test_data)

    x_range = map_range[0]
    y_range = map_range[1]
    particle_num = 200

    test_record = []
    predict_path = []
    real_path = []
    dists = []
    count = 1

    avg_time = []

    for i in np.arange(0, len(test_data), 1):
        # mag_x = test_data[i][0][:, 0]
        # mag_y = test_data[i][0][:, 1]
        # mag_z = test_data[i][0][:, 2]
        # mags = np.sqrt(np.array(mag_x, dtype='float') ** 2
        #               + np.array(mag_y, dtype='float') ** 2
        #               + np.array(mag_z, dtype='float') ** 2)
        mags = test_data[i][2]
        # mag_filter = mean_filter(mags, filter_size=10).tolist()
        # mag_filter = (mag_filter - np.average(mag_filter)).tolist()

        # acc = np.sqrt(np.array(test_data[i][1][:, 0], dtype='float') ** 2
        #               + np.array(test_data[i][1][:, 1], dtype='float') ** 2
        #               + np.array(test_data[i][1][:, 2], dtype='float') ** 2)

        gyro_z = np.random.normal(0, 0.0000, size=500)

        # step_peaks = stepEstimate.step_detect(acc)

        step_lens = stepEstimate.step_lens_norm(STEP_LENGTH,
                                                STEP_ESTIMATE_NOISE,
                                                size=particle_num) + random.gauss(0.0, 0.1)# * (1 - STEP_PARA) + STEP_LENGTH * STEP_PARA

        real_pos = test_data[i][1]

        pos_flag_1 = 0 if (test_data[i][0][0] == test_data[i][1][0]) else 1

        pos_flag_2 = 0 if (test_data[i][0][0] > test_data[i][1][0] or test_data[i][0][1] > test_data[i][1][1]) else 1

        #if test_data[i][0][0] > test_data[i][1][0] or test_data[i][0][1] > test_data[i][1][1]:
         #   step_lens = step_lens * (-1)

        rad = 8
        particles = PF.create_particles(num=particle_num, x_range=[test_data[i][0][0]-rad, test_data[i][0][0]+rad],
                                       y_range=[test_data[i][0][1]-rad, test_data[i][0][1]+rad],
                                       flag=[pos_flag_1, pos_flag_2])

        # particles = PF.create_particles(particle_num, x_range, y_range, flag=[pos_flag_1, pos_flag_2])

        weights = np.ones(particle_num) / particle_num

        predict_pos = PF.estimate_average(particles=particles, weights=weights)[0:2]

        # for idx in range(len(step_peaks) - 5):
        #     start = int(step_peaks[idx])
        #     end = int(step_peaks[idx + 5])

        # start = int(np.fabs(0 + int(np.random.normal(0, STEP_MAG_LEN/2, size=1))))
        # end = min(499, start + STEP_MAG_LEN*5)
        a = time.time()
        start = 0
        end = start + STEP_MAG_LEN*step_num
        while end < 500 or end == 500:

            # if (step_end - step_start > 70) or (step_end - step_start < 50):
            #     continue

            turn_angle = np.sum(gyro_z[start:end]) * 0.0
            # turn_angle = 0
            PF.move_particles(
                particles=particles,
                weights=weights,
                heading_angle=turn_angle,
                x_range=x_range,
                y_range=y_range,
                invalid_area=map_invalid_area,
                heading_len=step_lens
            )

            mag = mags[start:end:10]
            # obv_mags = get_obv_mags(db_data, particles[:, 0:2])
            obv_pos, obv_mag = get_obv_pos(db_data, mag)

            PF.update_particles_1(
                particles=particles,
                weights=weights,
                obv_pos=obv_pos
            )

            # step_lens = stepEstimate.step_lens_norm(np.average(step_lens, weights=weights),
            #                                         STEP_ESTIMATE_NOISE,
            #                                         size=particle_num) * (1 - STEP_PARA) + STEP_LENGTH * STEP_PARA
            if max(weights) > 0.6:
                predict_particle = PF.estimate_max(particles=particles, weights=weights)
            else:
                predict_particle = PF.estimate_average(particles=particles, weights=weights)
            # print("第%d步的预测位置为（%f, %f）" % (index + 1, predict_pos[0], predict_pos[1]))
            predict_pos = [predict_particle[0], predict_particle[1]]

            particles, weights = PF.resample_particles(
                particles=particles,
                weights=weights,
                num=particle_num,
                heading_angle=turn_angle,
                previous_res=predict_particle
            )

            start = start + 50
            end = start + STEP_MAG_LEN*step_num #+ int(np.random.normal(0, STEP_MAG_LEN/10, size=1))
        b = time.time()
        avg_time.append(b - a)
        # print(b - a)
        dist = np.linalg.norm(np.array(predict_pos) - np.array(real_pos))

        print("第%d段数据:\n  真实位置：(%f, %f)\n  预测位置：(%f, %f)\n  预测误差为：%f 米"
              % (count, real_pos[0], real_pos[1], predict_pos[0], predict_pos[1], dist))
        predict_path.append(predict_pos)
        real_path.append(real_pos)
        dists.append(dist)
        test_record.append(i)
        count = count + 1

    print("平均定位误差：%f" % np.average(dists))
    print("average process time: {:.4}".format(np.average(avg_time)))

    file_save = RES_PATH + "Magicol_" + map_name + ".csv"
    if not os.path.exists(file_save):
        f = open(file_save, "w")
        f.write("test_seg,real_pos_x,real_pos_y,predict_pos_x,predict_pos_y,location_error\n")
        f.close()
    f = open(file_save, "r")
    test_case = []
    for line in f.readlines():
        line = line.strip().split(",")
        test_case.append(line[0])
    f.close()

    f = open(file_save, 'a+')
    for k in range(len(dists)):
        if str(test_record[k]) in test_case:
            continue
        f.write(str(test_record[k]) + ","
                + str(real_path[k][0]) + ","
                + str(real_path[k][1]) + ","
                + str(predict_path[k][0]) + ","
                + str(predict_path[k][1]) + ","
                + str(dists[k]) + "\n")
    f.close()

    ecdf = sm.distributions.ECDF(np.array(dists))
    x = np.linspace(0, 12, num=100)
    y = ecdf(x)
    plt.figure("Magicol cdf")
    plt.xlim(0, 12)
    plt.ylim(0, 1)
    plt.plot(x, y)
    # plt.show()

    plt.figure("Magicol track")
    predict_path = np.array(predict_path)
    real_path = np.array(real_path)
    plt.scatter(predict_path[:, 0], predict_path[:, 1])
    plt.scatter(real_path[:, 0], real_path[:, 1], edgecolors='red')
    plt.show()

    pos = np.array(real_path)
    pos_x = pos[:, 0]
    pos_y = pos[:, 1]
    colors = (np.linalg.norm(np.array(real_path) - np.array(predict_path), axis=1)).tolist()
    # colors = normalize_func(colors)
    plt.scatter(pos_x, pos_y, c=colors)
    plt.colorbar()
    plt.grid(True)
    plt.show()

