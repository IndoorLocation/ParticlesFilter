#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @File  : MaLocTest.py
# @Author: HT
# @Date  : 2018/6/3
# @Desc  :

import numpy as np
import random
import PF
import os
from MaLoc import stepEstimate
from util.readExcel import get_data_by_sheet
from util.meanFilter import mean_filter
from util.plot import plot_cdf
from MaLoc.dbEstablish import generate_db_data_2
from MaLoc.observation import get_obv_pos, get_obv_mags
from matplotlib import pyplot as plt
from MapsFeatures import *
from DataProcess.generateTestData import generate_test_data_gogo
import statsmodels.api as sm # recommended import according to the docs
from util.readPKL import get_pkl_data

STEP_LENGTH = 0.55
STEP_ESTIMATE_NOISE = 0.01
STEP_PARA = 0.5

# map_range = chaosuan_range
# map_invalid_area = chaosuan_invalid_area
# map_name = "chaosuan"

map_range = gogo_range_3
map_invalid_area = gogo_invalid_area_3
map_name = "gogo"

seg_len = 1024
seg_interval = 128


if __name__ == "__main__":
    sensor_data, loc_start_data, loc_end_data = get_pkl_data(pkl_file_mate)
    # db_data = generate_db_data_2(sensor_data[50:1400], loc_start_data[50:1400], loc_end_data[50:1400])
    # test_data = generate_test_data_gogo(sensor_data[1450:], loc_start_data[1450:], loc_end_data[1450:])

    db_data = generate_db_data_2(sensor_data[0:2531:50], loc_start_data[0:2531:50], loc_end_data[0:2531:50])
    test_data = generate_test_data_gogo(sensor_data[2795:5328], loc_start_data[2795:5328], loc_end_data[2795:5328])

    x_range = map_range[0]
    y_range = map_range[1]
    particle_num = 3000

    test_record = []
    predict_path = []
    real_path = []
    dists = []
    count = 1

    for i in np.arange(0, len(test_data), 33):
        mag_x = test_data[i][0][:, 0]
        mag_y = test_data[i][0][:, 1]
        mag_z = test_data[i][0][:, 2]

        acc = np.sqrt(np.array(test_data[i][1][:, 0], dtype='float') ** 2
                      + np.array(test_data[i][1][:, 1], dtype='float') ** 2
                      + np.array(test_data[i][1][:, 2], dtype='float') ** 2)

        gyro_z = test_data[i][2][:, 2]

        # step_peaks = stepEstimate.step_detect(acc)

        step_lens = stepEstimate.step_lens_norm(STEP_LENGTH,
                                                STEP_ESTIMATE_NOISE,
                                                size=particle_num) * (1 - STEP_PARA) + STEP_LENGTH * STEP_PARA

        real_pos = test_data[i][4]

        start_pos = test_data[i][5]

        # particles = PF.create_particles(num=particle_num, x_range=x_range,
        #                                 y_range=y_range)

        # particles = PF.create_particles(num=particle_num, x_range=[start_pos[0] - 2, start_pos[0] + 2],
        #                                 y_range=[start_pos[1] - 2, start_pos[1] + 2])

        particles = PF.create_particles(particle_num, x_range, y_range)

        weights = np.ones(particle_num) / particle_num

        predict_pos = PF.estimate_average(particles=particles, weights=weights)[0:2]

        # for idx in range(len(step_peaks) - 1):
        #     step_start = int(step_peaks[idx])
        #     step_end = int(step_peaks[idx+1])

        step_start = int(np.fabs(0 + int(np.random.normal(0, 10, size=1))))
        step_end = min(1023, step_start + 63 + int(np.random.normal(0, 5, size=1)))
        while step_end < 1023:
            # step_start =
            # step_end = min(1023, step_start + 63 + int(np.random.normal(0, 10, size=1)))
            # if (step_end - step_start > 70) or (step_end - step_start < 50):
            #     continue
            turn_angle = np.sum(gyro_z[step_start:step_end]) * 0.01
            # turn_angle = 0
            PF.move_particles(
                particles=particles,
                weights=weights,
                heading_angle=turn_angle,
                x_range=x_range,
                y_range=y_range,
                invalid_area=map_invalid_area,
                heading_len=step_lens
            )

            mag = [mag_x[step_end], mag_y[step_end], mag_z[step_end]]
            # obv_mags = get_obv_mags(db_data, particles[:, 0:2])
            obv_pos = get_obv_pos(db_data, mag)

            PF.update_particles_1(
                particles=particles,
                weights=weights,
                obv_pos=obv_pos
            )

            # PF.update_particles_3(
            #     particles=particles,
            #     weights=weights,
            #     obv_mags=obv_mags,
            #     mag=mag,
            #     obv_pos=[]
            # )

            step_lens = stepEstimate.step_lens_norm(np.average(step_lens, weights=weights),
                                                    STEP_ESTIMATE_NOISE,
                                                    size=particle_num) * (1 - STEP_PARA) + STEP_LENGTH * STEP_PARA
            if max(weights) > 0.6:
                predict_particle = PF.estimate_max(particles=particles, weights=weights)
            else:
                predict_particle = PF.estimate_average(particles=particles, weights=weights)
            # print("第%d步的预测位置为（%f, %f）" % (index + 1, predict_pos[0], predict_pos[1]))
            predict_pos = [predict_particle[0], predict_particle[1]]

            particles, weights = PF.resample_particles(
                particles=particles,
                weights=weights,
                num=particle_num,
                heading_angle=turn_angle,
                previous_res=predict_particle
            )

            step_start = step_end + 1
            step_end = step_start + 63 + int(np.random.normal(0, 5, size=1))

        dist = np.linalg.norm(np.array(predict_pos) - np.array(real_pos)) * 0.5
        print("第%d段数据:\n  真实位置：(%f, %f)\n  预测位置：(%f, %f)\n  预测误差为：%f 米"
              % (count, real_pos[0], real_pos[1], predict_pos[0], predict_pos[1], dist))
        predict_path.append(predict_pos)
        real_path.append(real_pos)
        dists.append(dist)
        test_record.append(i)
        count = count + 1

    print("平均定位误差：%f" % np.average(dists))
    
    if not os.path.exists("MaLoc_" + map_name + ".csv"):
        f = open("MaLoc_" + map_name + ".csv", "w")
        f.write("test_seg,real_pos_x,real_pos_y,predict_pos_x,predict_pos_y,location_error\n")
        f.close()
    f = open("MaLoc_" + map_name + ".csv", "r")
    test_case = []
    for line in f.readlines():
        line = line.strip().split(",")
        test_case.append(line[0])
    f.close()

    f = open("MaLoc_" + map_name + ".csv", 'a+')
    for k in range(len(dists)):
        if str(test_record[k]) in test_case:
            continue
        f.write(str(test_record[k]) + ","
                + str(real_path[k][0]) + ","
                + str(real_path[k][1]) + ","
                + str(predict_path[k][0]) + ","
                + str(predict_path[k][1]) + ","
                + str(dists[k]) + "\n")
    f.close()

    ecdf = sm.distributions.ECDF(np.array(dists))
    x = np.linspace(0, 15, num=100)
    y = ecdf(x)
    plt.figure("Maloc cdf")
    plt.xlim(0, 15)
    plt.ylim(0, 1)
    plt.plot(x, y)
    # plt.show()

    plt.figure("Maloc track")
    predict_path = np.array(predict_path)
    real_path = np.array(real_path)
    plt.scatter(predict_path[:, 0], predict_path[:, 1])
    plt.scatter(real_path[:, 0], real_path[:, 1], edgecolors='red')
    plt.show()







