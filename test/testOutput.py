#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @File  : testOutput.py
# @Author: HT
# @Date  : 2018/6/20
# @Desc  :


# f = open("/test/test.csv", "w+")
# f.write("0,2,3,4\n")
# f.write("8,6,7,8\n")
# f.close()


def calculate(point_1, point_2):
    k = -1 * (point_2[1] - point_1[1]) / (point_2[0] - point_1[0])
    b = (k * point_1[0] + point_1[1]) * -1

    return k, b


# point_1 = [1.1, 23.57]
# point_2 = [-0.66, -7.89]
# k, b = calculate(point_1, point_2)
# print(calculate(point_1, point_2))

from Magicol.dbEstablish import *
from DataProcess.generateTestData import *
from Magicol.observation import *
from matplotlib import pyplot as plt

PLACE = "chaosuan"
PHONE = "SensorData_S7e"

# original data file path
TRAIN_DATA_PATH = "/home/hetao/Data/" + PLACE + "/" + PHONE + "/train/"
TEST_DATA_PATH = "/home/hetao/Data/" + PLACE + "/" + PHONE + "/test/"

db_data = generate_db_data_4(TRAIN_DATA_PATH)
test_data = generate_db_data_4(TEST_DATA_PATH)

res = []

for item in test_data:
    obv_pos, obv_mag = get_obv_pos(db_data, item[1])
    print(np.linalg.norm(np.array(item[0]) - np.array(obv_pos)))
    if np.linalg.norm(np.array(item[0]) - np.array(obv_pos)) > 20:
        plt.plot(item[1], color='red')
        plt.plot(obv_mag, color='green')
        plt.show()
        aaaaa = [item, [obv_pos, obv_mag]]
        res.append([item, [obv_pos, obv_mag]])



