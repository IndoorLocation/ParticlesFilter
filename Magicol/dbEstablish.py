#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @File  : dbEstablish.py
# @Author: HT
# @Date  : 2018/6/3
# @Desc  :

import numpy as np
import os
from DataProcess.coordinate import *
from util.readExcel import *
from util.meanFilter import mean_filter
from MapsFeatures import *

dist = 1


def generate_db_data_1(file, map_path, map_pos):
    original_data = get_data_by_sheet(file, sheet_index=0)
    mag_x = original_data[1][1:]
    mag_y = original_data[2][1:]
    mag_z = original_data[3][1:]
    mag = np.sqrt(np.array(mag_x, dtype='float')**2 + np.array(mag_y, dtype='float')**2 + np.array(mag_z, dtype='float')**2).tolist()

    pos = []

    # f = open("db_data.txt", 'w')
    for index in range(len(map_path)):
        path_start = map_path[index][0]
        path_end = map_path[index][1]
        pos_start = map_pos[index]
        pos_end = map_pos[index+1]
        x = mag_x[path_start:path_end:dist]
        y = mag_y[path_start:path_end:dist]
        z = mag_z[path_start:path_end:dist]
        for i in range(len(x)):
            pos_x = pos_start[0] + (pos_end[0] - pos_start[0]) * (i / len(x))
            pos_y = pos_start[1] + (pos_end[1] - pos_start[1]) * (i / len(x))
            pos.append([pos_x, pos_y])

    return pos, mag
    # return db_data


def generate_db_data_2(file, pos, step_mag_len=60):
    original_data = get_data_by_sheet(file, sheet_index=0)
    mag_x = original_data[1][1:]
    mag_y = original_data[2][1:]
    mag_z = original_data[3][1:]
    mag = np.sqrt(np.array(mag_x, dtype='float')**2
                  + np.array(mag_y, dtype='float')**2
                  + np.array(mag_z, dtype='float')**2)
    mag_filter = mean_filter(mag, filter_size=30)
    mag_filter = (mag_filter - np.average(mag_filter)).tolist()

    # f = open("db_data.txt", 'w')
    # for index in range(len(map_path)):
    #     path_len = map_path[index][1] - map_path[index][0]
    #     pos_start = map_pos[index]
    #     pos_end = map_pos[index+1]
    #     for i in range(path_len):
    #         pos_x = pos_start[0] + (pos_end[0] - pos_start[0]) * (i / path_len)
    #         pos_y = pos_start[1] + (pos_end[1] - pos_start[1]) * (i / path_len)
    #         pos.append([pos_x, pos_y])

    db_data = []
    mag_start = 0
    while(mag_start + step_mag_len * 5) < len(mag_filter) and (mag_start + step_mag_len * 5) < len(pos):
        db_data.append([pos[mag_start + step_mag_len * 5], mag_filter[mag_start:(mag_start + step_mag_len * 5):10]])
        # f.write(str([pos[mag_start + mag_len * 5], mag_filter[mag_start:(mag_start + mag_len * 5):10]]) + '\n')
        mag_start += int(step_mag_len/3)
    # f.close()
    return db_data


def generate_db_data_3(sensor_data, loc_start_data, loc_end_data, step_mag_len=60):
    db_data = []
    for idx in range(len(sensor_data)):
        mag_x = sensor_data[idx, :, 0]
        mag_y = sensor_data[idx, :, 1]
        mag_z = sensor_data[idx, :, 2]
        loc_start = loc_start_data[idx]
        loc_end = loc_end_data[idx]
        mag = np.sqrt(np.array(mag_x, dtype='float') ** 2
                      + np.array(mag_y, dtype='float') ** 2
                      + np.array(mag_z, dtype='float') ** 2)
        mag_filter = mean_filter(mag, filter_size=20).tolist()
        mag_filter = (mag_filter - np.average(mag_filter)).tolist()
        mag_start = 0
        while (mag_start + step_mag_len * 5) < len(mag_filter):
            loc = loc_start + (loc_end - loc_start) * (mag_start + step_mag_len * 5)/1024
            db_data.append([loc.tolist(), mag_filter[mag_start:(mag_start + step_mag_len * 5):10]])
            mag_start = mag_start + int(step_mag_len)

    return db_data


MAGS_LEN = 500
SAMPLE_INTERVAL = 50

step_num = 10
MAP_SCALE = 4.0


def generate_db_data_4(files_path, step_mag_len=30):
    db_data = []
    files = os.listdir(files_path)
    for file in files:
        if os.path.isdir(file):# or 'r-' in file or 'm-' in file or 'ml-' in file:
            continue
        # pos = get_pos_chaosuan(file)
        # pos = get_pos_gogo_floor2(file)
        # pos = get_pos_gogo_floor3(file)
        # pos = get_pos_jre(file)
        pos = get_pos_supermarket(file)
        # print(file)
        pos_start, pos_end = np.array(pos[0])/MAP_SCALE, np.array(pos[1])/MAP_SCALE
        mag_x = read_csv_by_col(files_path + file, "mag_x")
        mag_y = read_csv_by_col(files_path + file, "mag_y")
        mag_z = read_csv_by_col(files_path + file, "mag_z")
        mag = np.sqrt(np.array(mag_x, dtype=float) * np.array(mag_x, dtype=float)
                      + np.array(mag_y, dtype=float) * np.array(mag_y, dtype=float)
                      + np.array(mag_z, dtype=float) * np.array(mag_z, dtype=float))

        mag = mean_filter(mag, filter_size=25)
        # mag = (mag - np.average(mag))

        for idx in range(0, len(mag) - step_mag_len*step_num, 50):
            loc = pos_start + (pos_end - pos_start) * ((idx + step_mag_len * step_num) / len(mag))
            db_data.append([loc.tolist(), mag[idx:(idx + step_mag_len * step_num):10].tolist()])
    return db_data


# PLACE = "gogo_floor3"
# PHONE = "SensorData_S7e"
#
# # original data file path
# TRAIN_DATA_PATH = "/home/hetao/Data/" + PLACE + "/" + PHONE + "/train/"
# TEST_DATA_PATH = "/home/hetao/Data/" + PLACE + "/" + PHONE + "/test/"
# print(np.array(generate_db_data_4(TRAIN_DATA_PATH)))


