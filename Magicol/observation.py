#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @File  : observation.py
# @Author: HT
# @Date  : 2018/6/3
# @Desc  :

import numpy as np
from util.fastdtw import fastdtw


def get_dtw_dist(mag_1, mag_2):
    rad = min(len(mag_1), len(mag_2))
    return fastdtw(mag_1, mag_2, radius=int(rad))[0]


def find_idx(db, pos):
    idx = 0
    dist = (db[0][0] - pos[0])**2 + (db[0][1] - pos[1])**2
    for i in range(len(db)):
        tmp_dist = (db[i][0] - pos[0])**2 + (db[i][1] - pos[1])**2
        if tmp_dist < dist:
            dist = tmp_dist
            idx = i
    return idx


def get_dist(db_pos, db_mag, particles, particles_last, mag=[]):
    pos = particles[:, 0:2]
    pos_last = particles_last[:, 0:2]
    dist = []

    for idx in range(len(pos)):
        start = int(find_idx(db_pos, pos_last[idx]))
        end = int(find_idx(db_pos, pos[idx]))
        if np.fabs(end - start) < 10 or np.fabs(end - start) > 150:
            dist.append(100)
            continue
        map_mag = db_mag[min(start, end): max(start, end)+1]
        if start > end:
            map_mag.reverse()
        dist.append(get_dtw_dist(map_mag, mag))

    return dist


def get_obv_pos(db_data, mag):
    idx = 0
    dist = get_dtw_dist(mag, db_data[0][1])
    for i in range(len(db_data)):
        item = db_data[i]
        tmp_dist = get_dtw_dist(mag, item[1])
        if tmp_dist < dist:
            dist = tmp_dist
            idx = i
    return db_data[idx][0], db_data[idx][1]


def get_obv_pos_dist(db_data, mag):
    idx = 0
    dist = np.linalg.norm(np.array(mag) - np.array(db_data[0][1]))
    for i in range(len(db_data)):
        item = db_data[i]
        tmp_dist = np.linalg.norm(np.array(mag) - np.array(item[1]))
        if tmp_dist < dist:
            dist = tmp_dist
            idx = i
    return db_data[idx][0], db_data[idx][1]


def get_obv_dists(db_data, particles_pos, obv_mag):
    dists = []
    for j in range(len(particles_pos)):
        pos = particles_pos[j]
        idx = 0
        dist = 10000
        for i in range(len(db_data)):
            item = db_data[i]
            tmp_dist = (float(pos[0]) - float(item[0][0])) ** 2 + (float(pos[1]) - float(item[0][1])) ** 2
            if tmp_dist < dist:
                dist = tmp_dist
                idx = i
            else:
                continue

        dists.append(get_dtw_dist(db_data[idx][1], obv_mag))

    return dists
    # return (np.array(dists) - min(dists)) / (max(dists) - min(dists))


def get_map_mag(db_data, pos):
    idx = 0
    dist = 1000.0
    for i in range(len(db_data)):
        map_pos = db_data[i][0]
        tmp_dist = (float(pos[0]) - float(map_pos[0]))**2 + (float(pos[1]) - float(map_pos[1]))**2
        if tmp_dist < dist:
            dist = tmp_dist
            idx = i
    return db_data[idx][1]

