#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @File   : coordinate.py
# @Author : HT
# @Date   : 18-9-10
# @Desc   : the coordinate of path's start and end points


# gogo floor 3
path_1 = [[1200, 650], [1200, 160]]
path_2 = [[830, 750], [830, 210]]
path_3 = [[640, 720], [640, 140]]
path_4 = [[450, 710], [450, 140]]
path_5 = [[370, 720], [370, 340]]
path_6 = [[1150, 230], [450, 230]]
path_7 = [[1170, 480], [370, 480]]
path_8 = [[1340, 650], [350, 650]]
path_9 = [[1340, 750], [300, 720]]


def get_pos_gogo_floor3(file_name):
    if "path 1-" in file_name:
        return path_1
    if "path 1r-" in file_name:
        return [path_1[1], path_1[0]]

    if "path 2-" in file_name:
        return path_2
    if "path 2r-" in file_name:
        return [path_2[1], path_2[0]]

    if "path 3-" in file_name:
        return path_3
    if "path 3r-" in file_name:
        return [path_3[1], path_3[0]]

    if "path 4-" in file_name:
        return path_4
    if "path 4r-" in file_name:
        return [path_4[1], path_4[0]]

    if "path 5-" in file_name:
        return path_5
    if "path 5r-" in file_name:
        return [path_5[1], path_5[0]]

    if "path 6-" in file_name:
        return path_6
    if "path 6r-" in file_name:
        return [path_6[1], path_6[0]]

    if "path 7-" in file_name:
        return path_7
    if "path 7r-" in file_name:
        return [path_7[1], path_7[0]]

    if "path 8-" in file_name:
        return path_8
    if "path 8r-" in file_name:
        return [path_8[1], path_8[0]]

    if "path 9-" in file_name:
        return path_9
    if "path 9r-" in file_name:
        return [path_9[1], path_9[0]]


# chaosuan floor 5
path_1_l = [[1740, 1045], [1740, 565]]
path_1_m = [[1750, 1045], [1750, 565]]
path_1_r = [[1760, 1045], [1760, 565]]

path_2_l = [[1750, 570], [955, 570]]
path_2_r = [[1750, 560], [955, 560]]

path_3_l = [[982, 565], [982, 1045]]
path_3_ml = [[964, 565], [964, 1045]]
path_3_mr = [[946, 565], [946, 1045]]
path_3_r = [[928, 565], [928, 1045]]

path_4_l = [[955, 1040], [1750, 1040]]
path_4_r = [[955, 1050], [1750, 1050]]


def get_pos_chaosuan(file_name):
    if "path 1l" in file_name:
        return path_1_l
    if "path 1m" in file_name:
        return path_1_m
    if "path 1r" in file_name:
        return path_1_r

    if "path 2l" in file_name:
        return path_2_l
    if "path 2r" in file_name:
        return path_2_r

    if "path 3l" in file_name:
        return path_3_l
    if "path 3ml" in file_name:
        return path_3_ml
    if "path 3mr" in file_name:
        return path_3_mr
    if "path 3r" in file_name:
        return path_3_r

    if "path 4l" in file_name:
        return path_4_l
    if "path 4r" in file_name:
        return path_4_r


# JRE underground
jre_path_3_l = [[180, 430], [1210, 430]]
jre_path_3_m = [[180, 460], [1210, 460]]
jre_path_3_r = [[180, 490], [1210, 490]]

jre_path_4_l = [[370, 240], [1240, 240]]
jre_path_4_m = [[370, 265], [1240, 265]]
jre_path_4_r = [[370, 290], [1240, 290]]

jre_path_5_l = [[120, 140], [360, 140]]
jre_path_5_m = [[120, 180], [360, 180]]
jre_path_5_r = [[120, 220], [360, 220]]

jre_path_6_l = [[180, 140], [180, 490]]
jre_path_6_m = [[150, 140], [150, 490]]
jre_path_6_r = [[120, 140], [120, 490]]

jre_path_7_l = [[445, 430], [445, 660]]
jre_path_7_m = [[415, 430], [415, 670]]
jre_path_7_r = [[385, 430], [385, 670]]


def get_pos_jre(file_name):
    if "path 3l" in file_name:
        return jre_path_3_l
    if "path 3m" in file_name:
        return jre_path_3_m
    if "path 3r" in file_name:
        return jre_path_3_r

    if "path 4l" in file_name:
        return jre_path_4_l
    if "path 4m" in file_name:
        return jre_path_4_m
    if "path 4r" in file_name:
        return jre_path_4_r

    if "path 5l" in file_name:
        return jre_path_5_l
    if "path 5m" in file_name:
        return jre_path_5_m
    if "path 5r" in file_name:
        return jre_path_5_r

    if "path 6l" in file_name:
        return jre_path_6_l
    if "path 6m" in file_name:
        return jre_path_6_m
    if "path 6r" in file_name:
        return jre_path_6_r

    if "path 7l" in file_name:
        return jre_path_7_l
    if "path 7m" in file_name:
        return jre_path_7_m
    if "path 7r" in file_name:
        return jre_path_7_r


# gogo floor 2
path_h1 = [[960, 540], [200, 540]]
path_h2 = [[960, 460], [200, 460]]
path_h3 = [[960, 380], [200, 380]]
path_h4 = [[960, 290], [440, 290]]

path_v1 = [[960, 540], [960, 290]]
path_v2 = [[750, 540], [750, 290]]
path_v3 = [[580, 540], [580, 290]]
path_v4 = [[390, 540], [390, 380]]
path_v5 = [[200, 540], [200, 380]]


def get_pos_gogo_floor2(file_name):
    if "path h1" in file_name:
        return path_h1
    if "path h2" in file_name:
        return path_h2
    if "path h3" in file_name:
        return path_h3
    if "path h4" in file_name:
        return path_h4

    if "path v1" in file_name:
        return path_v1
    if "path v2" in file_name:
        return path_v2
    if "path v3" in file_name:
        return path_v3
    if "path v4" in file_name:
        return path_v4
    if "path v5" in file_name:
        return path_v5


# Supermarket
path_l1 = [[13, 0], [140, 0]]
path_l2 = [[59, 21], [140, 21]]
path_l3 = [[0, 42], [140, 42]]

path_s1 = [[26, 0], [26, 42]]
path_s2 = [[39, 0], [39, 42]]
path_s3 = [[53, 0], [53, 42]]
path_s4 = [[67, 0], [67, 42]]
path_s5 = [[80, 0], [80, 42]]
path_s6 = [[94, 0], [94, 42]]
path_s7 = [[108,0], [108,42]]
path_s8 = [[122,0], [122,42]]
path_s9 = [[140,0], [140,42]]


def get_pos_supermarket(file_name):
    if "path l1" in file_name:
        return path_l1
    if "path l2" in file_name:
        return path_l2
    if "path l3" in file_name:
        return path_l3

    if "path s1" in file_name:
        return path_s1
    if "path s2" in file_name:
        return path_s2
    if "path s3" in file_name:
        return path_s3
    if "path s4" in file_name:
        return path_s4
    if "path s5" in file_name:
        return path_s5
    if "path s6" in file_name:
        return path_s6
    if "path s7" in file_name:
        return path_s7
    if "path s8" in file_name:
        return path_s8
    if "path s9" in file_name:
        return path_s9

