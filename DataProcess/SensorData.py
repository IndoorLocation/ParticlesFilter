#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @File  : SensorData.py
# @Author: HT
# @Date  : 2018/6/30
# @Desc  : Sensor data class for one segment


import numpy as np

class SensorData(object):
    def __init__(self):
        self.reset()

    def reset(self):
        self.mags = []
        self.acccs = []
        self.gyros = []
        self.orien = []

    def get_sensor_data(self, one_segment):
        sensor_data = one_segment
        mags = np.reshape(sensor_data[:, :, 1:4], (len(sensor_data), 3))
        accs = np.reshape(sensor_data[:, :, 4:7], (len(sensor_data), 3))
        gyros = np.reshape(sensor_data[:, :, 7:10], (len(sensor_data), 3))
