#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @File  : generateTestData.py
# @Author: HT
# @Date  : 2018/6/11
# @Desc  : generate test magnetic data with specific length


from util.readExcel import get_data_by_sheet
from MapsFeatures import *
from util.meanFilter import mean_filter
from util.readExcel import *
from DataProcess.coordinate import *
import xlrd
import xlwt
import os

def get_all_pos(map_path, map_pos):
    pos = []
    # f = open("db_data.txt", 'w')
    for index in range(len(map_path)):
        path_len = int(map_path[index][1] - map_path[index][0])
        pos_start = map_pos[index]
        pos_end = map_pos[index + 1]
        for i in range(path_len):
            pos_x = pos_start[0] + (pos_end[0] - pos_start[0]) * (i / path_len)
            pos_y = pos_start[1] + (pos_end[1] - pos_start[1]) * (i / path_len)
            pos.append([pos_x, pos_y])
    return pos


def test_data_mag_seg(mag_data, pos, seg_len, interval):
    seg_start = 0
    test_data = []
    while seg_start + seg_len < len(mag_data) and seg_start + seg_len < len(pos):
        test_data.append([mag_data[seg_start:seg_start+seg_len], pos[seg_start+seg_len], seg_start+seg_len])
        seg_start = seg_start + interval
    return test_data


def test_data_step_seg(data, seg_len):
    step_len = min(len(data[0]), len(data[1]), len(data[2]))
    seg_start = 0
    test_data = []
    while seg_start + seg_len < step_len:
        test_data.append(data[:, seg_start:(seg_start + seg_len)])
        seg_start = seg_start + seg_len
    return test_data


def generate_test_data_gogo(sensor_data, loc_start_data, loc_end_data):
    test_data = []

    for i in np.arange(0, len(sensor_data)):
        test_data.append([sensor_data[i, :, 0:3],
                          sensor_data[i, :, 3:6],
                          sensor_data[i, :, 6:9],
                          sensor_data[i, :, 9:12],
                          loc_end_data[i],
                          loc_start_data[i]])

    return test_data


MAGS_LEN = 500
SAMPLE_INTERVAL = 50
MAP_SCALE = 4.0


def generate_test_data(files_path):
    test_data = []
    files = os.listdir(files_path)
    for file in files:
        if os.path.isdir(file):# or 'r-' in file or 'm-' in file or 'ml-' in file:
            continue
        # pos = get_pos_chaosuan(file)
        # pos = get_pos_gogo_floor2(file)
        # pos = get_pos_gogo_floor3(file)
        # pos = get_pos_jre(file)
        pos = get_pos_supermarket(file)
        # print(file)
        pos_start, pos_end = np.array(pos[0])/MAP_SCALE, np.array(pos[1])/MAP_SCALE
        mag_x = read_csv_by_col(files_path + file, "mag_x")
        mag_y = read_csv_by_col(files_path + file, "mag_y")
        mag_z = read_csv_by_col(files_path + file, "mag_z")
        mag = np.sqrt(np.array(mag_x, dtype=float) * np.array(mag_x, dtype=float)
                      + np.array(mag_y, dtype=float) * np.array(mag_y, dtype=float)
                      + np.array(mag_z, dtype=float) * np.array(mag_z, dtype=float))

        mag = mean_filter(mag, filter_size=25)
        # mag = (mag - np.average(mag))

        for idx in range(0, len(mag) - MAGS_LEN, SAMPLE_INTERVAL):
            loc_start = pos_start + (pos_end - pos_start) * ((idx + 250) / len(mag))
            loc = pos_start + (pos_end - pos_start) * ((idx + MAGS_LEN) / len(mag))
            test_data.append([loc_start.tolist(), loc.tolist(), mag[idx:idx + MAGS_LEN].tolist()])
    return test_data



