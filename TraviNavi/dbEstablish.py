#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @File  : dbEstablish.py
# @Author: HT
# @Date  : 2018/6/27
# @Desc  :


import numpy as np
from util.readExcel import get_data_by_sheet
from util.meanFilter import mean_filter


def generate_db_data(file, pos, step_mag_len=60):
    original_data = get_data_by_sheet(file, sheet_index=0)
    mag_x = original_data[1][1:]
    mag_y = original_data[2][1:]
    mag_z = original_data[3][1:]
    mag = np.sqrt(np.array(mag_x, dtype='float') ** 2
                  + np.array(mag_y, dtype='float') ** 2
                  + np.array(mag_z, dtype='float') ** 2)
    mag_filter = mean_filter(mag, filter_size=30)
    mag_filter = (mag_filter - np.average(mag_filter)).tolist()

    # f = open("db_data.txt", 'w')
    # for index in range(len(map_path)):
    #     path_len = map_path[index][1] - map_path[index][0]
    #     pos_start = map_pos[index]
    #     pos_end = map_pos[index+1]
    #     for i in range(path_len):
    #         pos_x = pos_start[0] + (pos_end[0] - pos_start[0]) * (i / path_len)
    #         pos_y = pos_start[1] + (pos_end[1] - pos_start[1]) * (i / path_len)
    #         pos.append([pos_x, pos_y])

    db_data = []
    mag_start = 0
    while (mag_start + step_mag_len * 5) < len(mag_filter) and (mag_start + step_mag_len * 5) < len(pos):
        db_data.append([pos[mag_start + step_mag_len * 5], mag_filter[mag_start:(mag_start + step_mag_len * 5):10]])
        # f.write(str([pos[mag_start + mag_len * 5], mag_filter[mag_start:(mag_start + mag_len * 5):10]]) + '\n')
        mag_start += int(step_mag_len / 3)
    # f.close()
    return db_data


def generate_db_data_2(sensor_data, loc_start_data, loc_end_data):
    db_data = []

    for i in range(len(sensor_data)):
        loc_start = np.array(loc_start_data[i])
        loc_end = np.array(loc_end_data[i])
        mags = sensor_data[i, :, 0:3]
        for j in np.arange(0, len(mags), 5):
            mag = mags[j]
            loc = loc_start + (loc_end - loc_start) * (j / len(mags))
            db_data.append([loc, mag])
    return db_data

