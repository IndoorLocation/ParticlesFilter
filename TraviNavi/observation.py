#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @File  : observation.py
# @Author: HT
# @Date  : 2018/6/28
# @Desc  :

from util.fastdtw import fastdtw


def get_dtw_dist(list_1, list_2):
    rad = min(len(list_1), len(list_2))
    return fastdtw(list_1, list_2, radius=rad)


def get_obv_pos(db_data, mag):
    idx = 0
    dist = get_dtw_dist(mag, db_data[0][1])
    for i in range(len(db_data)):
        item = db_data[i]
        tmp_dist = get_dtw_dist(mag, item[1])
        if tmp_dist < dist:
            dist = tmp_dist
            idx = i
    return db_data[idx][0]

