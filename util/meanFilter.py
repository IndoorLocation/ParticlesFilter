#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @File  : meanFilter.py
# @Author: HT
# @Date  : 2018/6/3
# @Desc  :

import numpy as np


def mean_filter(orginal_list, filter_size=30):
    mylist = np.copy(orginal_list)
    for idx in range(filter_size):
        mylist[idx] = np.average(mylist[idx:(idx+filter_size)])

    for idx in range(filter_size, (mylist.__len__()-filter_size), 1):
        mylist[idx] = np.average(mylist[(idx - filter_size):(idx + filter_size)])

    for idx in range(mylist.__len__()-1, mylist.__len__() - filter_size, -1):
        mylist[idx] = np.average(mylist[(idx - filter_size):idx])
    return mylist