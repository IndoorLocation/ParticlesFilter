#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @File  : plot.py
# @Author: HT
# @Date  : 2018/6/11
# @Desc  : plot figures of data

import numpy as np
import statsmodels.api as sm # recommended import according to the docs
import matplotlib.pyplot as plt


def plot_cdf(data, x_range, x_num=100):
    if x_range is None:
        x_range = max(data)
    ecdf = sm.distributions.ECDF(np.array(data))
    x = np.linspace(0, x_range, num=x_num)
    y = ecdf(x)
    plt.xlim(0, x_range)
    plt.ylim(0, 1)
    plt.plot(x, y)
    # plt.legend()
    plt.show()


def get_cdf_plt(data, x_range, x_num=100, color=None, linestyle=None):
    if x_range is None:
        x_range = max(data)
    ecdf = sm.distributions.ECDF(np.array(data))
    x = np.linspace(0, x_range, num=x_num)
    y = ecdf(x)
    plt.xlim(0, x_range)
    plt.ylim(0, 1)
    plt_figure, = plt.plot(x, y)
    return plt_figure

