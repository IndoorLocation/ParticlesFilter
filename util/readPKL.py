#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @File  : readPKL.py
# @Author: HT
# @Date  : 2018/6/29
# @Desc  :


import pickle as pk
import numpy as np
import matplotlib.pyplot as plt
from MapsFeatures import pkl_file_chaosuan, pkl_file_mate


def get_pkl_data(file):
    f = open(file, 'rb')
    nosense, sensors_data, loc_data_1, loc_data_2 = pk.load(f)
    f.close()
    sensors_data = np.reshape(sensors_data[:, :, :, 1:13], (len(sensors_data), 1024, 12))
    loc_start_data = np.reshape(loc_data_2, (len(loc_data_2), 8))[:, 1:3]
    loc_end_data = np.reshape(loc_data_1, (len(loc_data_1), 8))[:, 1:3]
    return sensors_data, loc_start_data, loc_end_data






# file = "C:/Users/justk/Desktop/magData/gogo/chaosuan.pkl"
# pkl_data_1, pkl_data_2, pkl_data_3 = get_pkl_data(pkl_file_chaosuan)
#
# acc = np.sqrt(np.array(pkl_data_1[1400, :, 3], dtype='float') ** 2
#                       + np.array(pkl_data_1[1400, :, 4], dtype='float') ** 2
#                       + np.array(pkl_data_1[1400, :, 5], dtype='float') ** 2)

# mag_x = pkl_data_1[0, :, 0]
# mag_y = pkl_data_1[0, :, 1]
# mag_z = pkl_data_1[0, :, 2]
#
# for i in np.arange(1, len(pkl_data_1), 60):
#     mag_x = np.append(mag_x, pkl_data_1[i, :, 0])
#     mag_y = np.append(mag_y, pkl_data_1[i, :, 1])
#     mag_z = np.append(mag_z, pkl_data_1[i, :, 2])
#
# plt.figure(1)
# plt.plot(acc)
# plt.plot(mag_x)
# plt.plot(mag_y)
# plt.plot(mag_z)
# plt.show()
